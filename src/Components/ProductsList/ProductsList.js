import React,{PureComponent} from 'react';
import ProductCard from '../ProductCard';
import './ProductsList.style.scss';

class ProductsList extends PureComponent {
    render() { 
        const {products} = this.props;
        return (
            <ul className='products'>
                {products.map((product)=> <ProductCard key={product.productId} product={product}/> )}
            </ul>
        );
    }
}
 
export default ProductsList;