import React,{useState} from 'react';
import Basket from './Basket';
import './Header.style.scss';

const Header = ({basket}) => {
    const [visible, toggle ] = useState(false);
    const handleClick = () => {
        basket.length && toggle(!visible);  
    }

    return (
        <header role='banner'>{}
            <button onClick={handleClick} >Cart ({basket.length})</button>
            {visible && <Basket basket={basket}/>}
        </header>
    );
}
 
export default Header;