import React from 'react';
import './Basket.style.scss';

const Basket = ({basket}) => {
    return ( 
        <ul className='basket'>
            { basket.map(({productId,title,qty}) => <li key={productId} >{title} - qty {qty}</li>)}
        </ul>
     );
}
 
export default Basket;