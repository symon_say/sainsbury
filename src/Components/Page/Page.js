import React from 'react'
import './Page.style.scss';

const Page = ({children}) => {
    return (
        <div id='page'>{children}</div>
    );
}
 
export default Page;