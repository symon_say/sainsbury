import React,{useState} from 'react'
import './Img.style.scss';

const Img = ({src,alt}) => {
    const [loaded,setLoaded] = useState(false);
    const img = new Image();
    img.onload = function () {
        setLoaded(true);
    }
    img.src = src;

    return (
        <>
       {loaded? <img src={src} alt={alt} /> :<span>Loading...</span>}
       </>
    );
}
 
export default Img;