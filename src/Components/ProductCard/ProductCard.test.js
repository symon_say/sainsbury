import React from 'react';
import { shallow } from 'enzyme';
import ProductCard from './ProductCard';
import Sku from '../Sku';

describe('<ProductCard/>', () => {
    let wrapper; 
    
    describe('Given card with props', () => {

        beforeEach(() => {
            let props = { 
                sku: 'abcd', 
                title: 'shave it', 
                price: '100.00', 
                image: 'https://hulahop.com/mybestimage'
            }
            wrapper = shallow(<ProductCard product={props} />);
        });

        it('should render the card', () => {
            expect(wrapper.find('img')).toMatchSnapshot();
            expect(wrapper.find('button')).toMatchSnapshot();
            expect(wrapper.find(<Sku/>)).toMatchSnapshot();
        });
    });
})