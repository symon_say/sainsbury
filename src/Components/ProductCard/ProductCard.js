import React from 'react';
import Img from '../Img';
import Sku from '../Sku';
import Price from '../Price';
import AddToBasket from '../AddToBasket'; 
import './ProductCard.style.scss';

const ProductCard = ({product}) => {
    const { sku, title, price, image} = product;
    
    return ( 
        <li className='card'>
            <div className='card-body'>
                <Img src={image} alt={title}/> 
                <ul>
                    <li>{title},<Sku sku={sku}/></li>
                    <li><Price price={price}/></li>
                </ul>
                <AddToBasket product={product}>Add to Basket</AddToBasket>
            </div>
        </li>
    );
}
 
export default ProductCard;