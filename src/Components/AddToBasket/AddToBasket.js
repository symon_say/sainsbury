import React,{useContext} from 'react';
import { BasketContext } from '../../Context';
import './AddToBasket.style.scss';
 
const AddToBasket = ({product}) => {
    const { add } = useContext(BasketContext)
    const handleAddToBasket = (product) => {
        add(product);
    } 

    return ( 
        <button className='addBasket' onClick={()=>handleAddToBasket(product)}>Add To Basket</button> 
    );
}
 
export default AddToBasket;