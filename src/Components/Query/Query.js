import { useReducer, useEffect } from 'react';
import {getProducts} from '../../api/products';

const Query = ({children}) => {
    const [state, setState] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {loaded: false, fetching: false, data: [], error: null}
    )

    useEffect(() => {
        setState({fetching: true});
        getProducts().then(data=>{
            setState({data, fetching:false, loaded:true});
        }).catch(error =>{
            setState({ fetching:false, loaded:true,error});
        })
    },[]);

    return children(state)
}
 
export default Query;