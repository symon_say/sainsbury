const productsUrl = 'https://jsainsburyplc.github.io/front-end-test/products.json';

export const getProducts = () => {
    return fetch(productsUrl)
    .then(res => res.json())
    .then(data => {
        return data
    });
}