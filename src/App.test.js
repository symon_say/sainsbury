import React from 'react';
import { mount, shallow } from 'enzyme';
import App from './App';
import { getProducts } from './api/products';
import mockData from '../tests/mockProducts.json';
import Query from './Components/Query';

jest.mock('./api/products');

describe('<App/>', () => {
    let wrapper; 

    describe('Given App ', () => {

        afterAll(() => jest.resetAllMocks());

        beforeEach(() => {
            (getProducts).mockImplementation(() =>
                Promise.resolve({ data: mockData })
            );
            wrapper = shallow(<App/>);
            
            console.log(wrapper.debug());
        });

        it('should render the card', () => {
            expect(true).toBe(true);
        });
    });
})