import React,{ useState } from 'react'
import './App.scss';
import Query from './Components/Query';
import Page from './Components/Page';
import Header from './Components/Header';
import ProductsList from './Components/ProductsList';
import { BasketContext } from './Context';

const App = () => {
    const [basket, setBasket ] = useState([]); 

		const add = (product) =>{
				const index = basket.findIndex( p => p.productId === product.productId);
				if(index > -1){
					basket[index].qty++ ;
					setBasket([...basket]);
				}
				else{
					setBasket([...basket,{...product,qty:1}]);
				}
		}

		const remove = (product) =>{ }

		return (
			<BasketContext.Provider value={{basket,add ,remove}}>
				<main className="App">
					<Page>
						<Header basket={basket}/>
						<Query >
							{({data}) =>  <ProductsList products={data}/>}
						</Query>
					</Page>
				</main>
			</BasketContext.Provider>
		);
}

export default App;
